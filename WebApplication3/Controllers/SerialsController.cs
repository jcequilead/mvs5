﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication3.Models;

namespace WebApplication3.Controllers
{
    public class SerialsController : Controller
    {
        private TESTEntities db = new TESTEntities();

        // GET: Serials
        public ActionResult Index()
        {
            return View(db.Serial.ToList());
        }

        // GET: Serials/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Serial serial = db.Serial.Find(id);
            if (serial == null)
            {
                return HttpNotFound();
            }
            return View(serial);
        }

        // GET: Serials/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Serials/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Tabla,Actual,Final")] Serial serial)
        {
            if (ModelState.IsValid)
            {
                db.Serial.Add(serial);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(serial);
        }

        // GET: Serials/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Serial serial = db.Serial.Find(id);
            if (serial == null)
            {
                return HttpNotFound();
            }
            return View(serial);
        }

        // POST: Serials/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Tabla,Actual,Final")] Serial serial)
        {
            if (ModelState.IsValid)
            {
                db.Entry(serial).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(serial);
        }

        // GET: Serials/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Serial serial = db.Serial.Find(id);
            if (serial == null)
            {
                return HttpNotFound();
            }
            return View(serial);
        }

        // POST: Serials/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Serial serial = db.Serial.Find(id);
            db.Serial.Remove(serial);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
